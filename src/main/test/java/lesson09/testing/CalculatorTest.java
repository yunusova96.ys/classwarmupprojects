package lesson09.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

  private Calculator c;

  @BeforeEach
  public void init() {
    this.c = new Calculator();
  }

  @Test
  public void test1() {
    int a = 5;
    int b = 6;
    int expected = 11;
    int actual = c.add(a, b);
    assertEquals(expected, actual);
  }

  @Test
  public void test2() {
    int a = 5;
    int b = 6;
    int expected = 11;
    int actual = c.add(a, b);
    assertEquals(expected, actual);
  }
}
