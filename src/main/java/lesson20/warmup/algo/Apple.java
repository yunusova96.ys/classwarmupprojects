package lesson20.warmup.algo;

public class Apple {
  public final int size;

  public Apple(int size) {
    this.size = size;
  }

  @Override
  public String toString() {
    return "Apple{" +
        "size=" + size +
        '}';
  }
}
