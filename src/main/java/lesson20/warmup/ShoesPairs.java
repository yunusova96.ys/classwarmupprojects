package lesson20.warmup;

public class ShoesPairs {

    int calc(String origin) {
        int shoes = 0;
        int groups = 0;
        for (int i = 0; i < origin.length(); i++) {
            switch (origin.charAt(i)) {
                case 'R':
                    shoes += 1;
                    break;
                case 'L':
                    shoes -= 1;
                    break;
                default:
                    throw new IllegalArgumentException("wrong char");
            }
            if (shoes==0){
                groups++;
            }
        }
        return groups;
    }

}


