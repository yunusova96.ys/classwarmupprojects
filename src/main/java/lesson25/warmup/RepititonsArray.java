package lesson25.warmup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RepititonsArray {
//    Generate 50 random numbers
//    in the range [10..25]
//    calculate the number of repetitions
//for each number
//    find the number which appeared
//    the minimal number of occurrences


    private static void duplicated(int[] array) {
        int count = 0;
        List<Integer> duplicated = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == (array[j])) {
                    duplicated.add(array[j]);
                    count++;
                }
            }
        }
        System.out.println("duplicated numbers are:" + duplicated);
        System.out.println("count" + count);
    }

    private static void duplicated2(int[] array) {
        int count2 = 0;
        List<Integer> duplicated = new ArrayList<>();

        Set set = new HashSet();
       boolean isDuplicated = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (!set.add(array[j])) {
                    count2++;
                    duplicated.add(array[j]);
                }
            }
        }
        System.out.println("duplicated2 numbers are:" + duplicated);
        System.out.println("count" + count2);
    }

    public static void main(String[] args) {
        int[] array = new int[50];
        for (int i = 0; i < 50; i++) {
            int rand = (int) (Math.random() * 15 + 10);
            array[i] = rand;
        }
        duplicated(array);
        duplicated2(array);


    }
}
