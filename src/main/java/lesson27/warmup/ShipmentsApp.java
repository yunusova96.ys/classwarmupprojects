package lesson27.warmup;

public class ShipmentsApp {

    static int fix(int[] w) {
        int min = 0, max = 0;
        int count = 0;
        for (int i = 0; i < w.length; i++) {//1 1 1 1 16
            min = Math.min(min, w[i]);
            max = Math.max(max, w[i]);
        }
        while (max - min > 2) {
            min = min + (max - min) / 3;
            int equaling = (max - min) / 3;
            max = max - equaling;
            count += equaling;
        }

        return count;
    }

    public static void main(String[] args) {
        int[] a1 = {1, 1, 1, 1, 6};
        int[] a2 = {1, 1, 1, 1, 16};
        int[] a3 = {1, 1, 1, 1, 15};
        int[] a4 = {10, 20, 2, 3, 15};
        System.out.println(fix(a1)); // 4
        System.out.println(fix(a2)); // 12
        System.out.println(fix(a3)); // -1
        System.out.println(fix(a4)); // 15
    }
}
