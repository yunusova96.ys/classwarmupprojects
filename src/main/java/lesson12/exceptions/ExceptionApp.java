package lesson12.exceptions;

import java.util.Optional;

public class ExceptionApp {
    public static void main(String[] args) {
        Optional<Integer> value1 = strToInt("123");//optional.og(123)
        Optional<Integer> value2 = strToInt("123a");//optional.empty

        System.out.println(value1);
        System.out.println(value2);
    }

    private static Optional<Integer> strToInt(String s)  {
        try{
            int i = Integer.parseInt(s);

            return Optional.of(i);
        }catch (NumberFormatException e){
            return  Optional.empty();
        }

    }
}
