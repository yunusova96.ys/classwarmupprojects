package lesson12.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class WriteFile {
    public void  writeFile() throws IOException {
        Path path = Paths.get("./data", "sentences.txt");
        ArrayList<String> content = new ArrayList<>();
        //todo content adding here
        Files.write(path, content);
    }
}
