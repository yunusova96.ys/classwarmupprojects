package lesson12.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public List<String> readSubjFile() throws IOException {
        Path path = Paths.get("./data", "subj_verb.txt");
        List<String> linesSub = Files.readAllLines(path, StandardCharsets.UTF_8);


        return linesSub;
    }

    public List<String> readVerbFile() throws IOException {
        Path path = Paths.get("./data", "verb_obj.txt");
        List<String> linesVerb = Files.readAllLines(path, StandardCharsets.UTF_8);
         return linesVerb;
    }
}
