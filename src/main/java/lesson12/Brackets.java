package lesson12;

public class Brackets {
    public static void main(String[] args) {
        System.out.println(calc("((()()()))"));
    }


    public static int calc(String origin) {

//     (()())
      int curr = 0;
      int max = 0;

        for (char c : origin.toCharArray()) {
            if (c == '(') {
                curr++;
                //countMax=brackets;
            } else if (c == ')') {
                curr--;
            }
               if( max<curr){
                 max = curr;
                 //brackets--;
               }

        }
        return max;
    }

}
