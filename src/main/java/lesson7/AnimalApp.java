package lesson7;

import java.util.ArrayList;
import java.util.List;

public class AnimalApp {

    public static void main(String[] args) {


        List<Animal> animalList = new ArrayList<Animal>();
        Cat cat = new Cat("Jerry");
        Dog dog = new Dog("Rocky");
        Fish fish = new Fish("Nemo");
        Animal dragon = new Animal("Percy") {

            @Override
            public String toString() {
                return "Animal{" +
                        "Im a Dragon,my name is'" + name + '\'' +
                        '}';
            }
        };

        animalList.add(cat);
        animalList.add(dog);
        animalList.add(fish);
        animalList.add(dragon);

        System.out.println(animalList);
    }
}
