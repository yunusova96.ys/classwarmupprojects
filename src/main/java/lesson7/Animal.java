package lesson7;

public class Animal {
    public String name;


    public Animal(String name) {
        this.name = name.toUpperCase();
    }

    public void say() {
        System.out.println("Im an animal");
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                '}';
    }
}
