package lesson8.random;

import java.util.ArrayList;
import java.util.Random;

public class AreaMain {
    public static void main(String[] args) {
        Random random = new Random();
        int xr = (int) ((Math.random() * 10) + 1);
        int yr= (int) ((Math.random()*10)+1);


        ArrayList<Figure> figureArrayList = new ArrayList<Figure>();
        figureArrayList.add(new Circle(new Point(xr,yr), 3));
        figureArrayList.add(new Rectangle(new Point(xr,yr), new Point(xr,yr)));
        figureArrayList.add(new Triangle(new Point(xr,yr), new Point(xr,yr), new Point(xr,yr)));

        int total_area = 0;
        for (Figure f : figureArrayList) {

            total_area += f.area();
        }
        System.out.println(total_area);

    }
}
