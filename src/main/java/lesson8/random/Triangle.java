package lesson8.random;

public class Triangle  extends Figure {
    Point p1 = new Point(3,4);
    Point p2 = new Point(1,4);
    Point p3 = new Point(2,4);

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    public Point getP3() {
        return p3;
    }

    public void setP3(Point p3) {
        this.p3 = p3;
    }

    @Override
    public int area() {
        return p1.x*p1.y*3;
    }
}

