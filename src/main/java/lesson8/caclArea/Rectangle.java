package lesson8.caclArea;

public class Rectangle  extends  Figure{
    Point p1;
    Point p2 ;
    private  int area;

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public Rectangle(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }



    @Override
    public int area() {
        area = Math.abs(p1.x - p2.x) * Math.abs(p1.y - p2.y);
        return  area;

    }
}
