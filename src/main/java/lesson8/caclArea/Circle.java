package lesson8.caclArea;

import static java.lang.Math.PI;

public class Circle extends  Figure {
   private  Point center;
   private  int radius;
//   private  double area;

    public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
//        this.area = area;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }





    @Override
    public int area() {
       int area = (int) (PI*Math.pow(radius,2));
        return  area;
    }
}
