package lesson8.caclArea;

import java.util.ArrayList;

public class AreaMain {
    public static void main(String[] args) {
        ArrayList<Figure>  figures = new ArrayList<Figure>();
        figures.add(new Circle(new Point(1,2),3));
        figures.add(new Rectangle(new Point(1,2),new Point(1,3)));
        figures.add(new Triangle(new Point(1,3),new  Point(3,4),new Point(3,4)));

        int total_area = 0;
        for (Figure f : figures) {
            total_area+= f.area();
        }
        System.out.println(total_area);
    }
}
