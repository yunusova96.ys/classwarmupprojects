package lesson22.warmup;

public class GCDApp {
    int gcd(int a, int b) {

        if(a==b) return a;
        if(a==0) return b;
        if(b==0) return a;
        if(a>b){//9,3    9-3=6,3  6-3,3    3,3 3==3->3
            return gcd(a-b,b);
        }else {
            return gcd(a,b-a);
        }

    }
}
