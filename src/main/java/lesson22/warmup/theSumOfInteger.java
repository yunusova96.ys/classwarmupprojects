package lesson22.warmup;

public class theSumOfInteger {

    public static void main(String[] args) {
        int n= 30;
        int m= 3;
        int sum = countSum(n);
        int p= countFact(m);
        int fib = fibanocci(m);
        System.out.println(sum);
        System.out.println(p);
        System.out.println(fib);

    }

    private static int fibanocci(int m) {
        if(m<=1){
            return m;
        }else{
            return fibanocci(m-1)+fibanocci(m+2);
        }
    }

    private static int countFact(int n) {
        if(n==0){
            return 1;
        }else{
            return n*countFact(n-1);
        }
    }

    private static int countSum(int n) {
        if (n == 0) {
            return 0;
        } else {
            return n + countSum(n - 1);
        }
    }
}
