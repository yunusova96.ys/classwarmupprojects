package lesson22.warmup;

import java.util.Random;

public class TheSum {

  static int sum (int[] data,int length) {
    {
      if (length <= 0)
        return 0;
      return (sum(data, length- 1) + data[length - 1]);
    }
  }

  public static void main(String[] args) {
    int[] d = new Random().ints(1, 10)
        .limit(10).toArray();
    int sum = sum(d,d.length);
    System.out.println(sum);
  }
}
