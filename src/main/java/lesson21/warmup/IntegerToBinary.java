package lesson21.warmup;

import java.util.Scanner;

public class IntegerToBinary {
    public static void main(String[] args) {
        System.out.println("Please enter an integer:");
        int value = new Scanner(System.in).nextInt(); // 12
        // your code should be written here
        String binary = "";
        while (value > 0) {
            int remainder = value % 2;
            binary += remainder;
            value = value / 2;
        }


        System.out.println(binary); // 00001100
    }
}
