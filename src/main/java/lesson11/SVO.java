package lesson11;

import java.util.ArrayList;
import java.util.List;

public class SVO {

    //    List<String> subjects = list("Noel", "The cat", "The dog")
//    List<String> verbs =    list("wrote", "chased", "slept on")
////    List<String> objects =  list("the book","the ball","the bed")
    List<String> subjects = new ArrayList<String>();
    List<String> verbs = new ArrayList<String>();
    List<String> objects = new ArrayList<String>();


    public void fillingLists() {
        subjects.add("Noel");
        subjects.add("The cat");
        subjects.add("The dog");
        verbs.add("wrote");
        verbs.add("slept on");
        verbs.add("chased");
        objects.add("the book");
        objects.add("the ball");
        objects.add("the bed");

    }

    public String makeSentenceStructure(String sub, String verb, String obj) {
        String sentence;
        sentence = (sub.concat(" ").concat(verb).concat(" ").concat(obj).concat("\n"));

        return sentence;

    }

    public List<String> createRandomSentence() {
        fillingLists();
        final List<String> listSentence = new ArrayList<String>();
        for (String sub : subjects) {
            for (String verb : verbs) {
                for (String obj : objects) {
                    listSentence.add(makeSentenceStructure(sub, verb, obj));
                }
            }
        }
        return listSentence;

    }

//    public List<String> createByStreams() {
////        fillingLists();
////        final List<String> listSentence = new ArrayList<String>();
////        subjects.forEach(sub -> verbs.forEach(verb -> objects.forEach(obj ->
////                listSentence.add(makeSentenceStructure(sub, verb, obj));
////        )));
////        return listSentence;
//    }


}
