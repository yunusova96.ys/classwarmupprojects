package lesson10.SetTask;

import java.util.ArrayList;
import java.util.List;

public class SetListNamesTask {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("Ali");
        list.add("Alex");
        list.add("Alex");
        list.add("Jack");
        list.add("Suzy");
        list.add("Suzy");
        list.add("Suzy");
        list.add("Heisenberg");
        List<String> newList = new ArrayList<String>();

        for (int i = 0; i < 50; i++) {
            int index = (int) ((Math.random() * list.size()));
            newList.add(list.get(index));

        }
        System.out.println(newList);
    }
}
