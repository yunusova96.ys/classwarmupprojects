package lesson10.SetTask;

import java.util.*;

public class SetListApp {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 20; i++) {
            int random = (int) ((Math.random() * 20) - 10);
            list.add(random);
        }
        System.out.println("list" + list);
        Set<Integer> set = new HashSet<Integer>(list);
        System.out.println("set" + set);
        List<Integer> newList = new ArrayList<Integer>(set);
        Collections.sort(newList);
        System.out.println("last list" + newList);
    }


}
