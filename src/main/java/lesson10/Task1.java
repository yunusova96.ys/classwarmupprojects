package lesson10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task1 {
    List<Character> list = new ArrayList<Character>();
    String word = "Hello world";

    int count = 0;
    Set<Character> setChar = new HashSet<Character>();

    public List<Character> getUniqueString(String s) {
        for (int i = 1; i < s.length(); i++) {
            setChar.add(s.charAt(i));
        }
        list.addAll(setChar);
        return list;

    }

    public List<Integer> sendUniqueToCountAll(String word) {
        List<Character> unique = getUniqueString(word);

        return countAll(word, unique);

    }

    public List<Integer> countAll(String word, List<Character> list) {
        List<Integer> integerList = new ArrayList<Integer>();

        for (Character character : list) {
            int countchar = countLetter(word, character);
            integerList.add(countchar);
        }

        return integerList;
    }


    public int countLetter(String word, Character letter) {
        for (int i = 1; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                count++;
            }

        }
        return count;
    }

}
