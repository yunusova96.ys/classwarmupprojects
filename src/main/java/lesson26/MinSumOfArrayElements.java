package lesson26;

import java.util.Arrays;

//minimal pair in random array
//        ----------------------------
//        1.generate random array range [10, 50] count = 30
//        2. find minimal sum of adjacent pair in the array
//        print it index
//        print the elements
//        print their sum
//
//        [26, 35, 93, 38, 5, 7, 18, 33, 7, 1, 70]
//        index: 8
//        num1: 7
//        num2: 1
//        sum:  8
public class MinSumOfArrayElements {


    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < 5; i++) {
            int rand = (int) (Math.random() * 15 + 10);
            array[i] = rand;
        }



    }



    private static int adjacement(int[] array) {
        int min = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                 sum = array[i] + array[j];
                System.out.println(sum);
                System.out.println(min);
            }
            min = Math.min(min, sum);
        }
        return min;
    }
}
